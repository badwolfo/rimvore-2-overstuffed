﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HarmonyLib;
using RimVore2;
using Verse;

namespace RV2_Overstuffed
{
    [HarmonyPatch(typeof(VoreTypeDef), "AreVoreEnablersValid")]
    public class Patch_VoreTypeDef_AreVoreEnablersValid
    {
        [HarmonyPostfix]
        public static void SkipTypeCheckForAssimilationAndAmalgamation(ref bool __result, Pawn predator)
        {
            try
            {
                QuirkManager quirks = predator.QuirkManager();
                if (quirks != null && quirks.HasSpecialFlag("UnlockAllVoreTypes"))
                {
                    __result = true;
                }
            }
            catch (Exception ex)
            {
                Log.Message("Something went wrong: " + ex.ToString());
            }
        }
    }
}
